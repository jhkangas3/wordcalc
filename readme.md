# Calculator using natural words

This calculator can perform operations using English words for numbers and operations. Valid numbers are in range from 0 to 20.
List of accepted words for numbers are
`zero`,`one`,`two`,`three`,`four`,`five`,`six`,`seven`,`eight`,`nine`,`ten`,`eleven`,`thirteen`,`fourteen`,`fifteen`,`sixteen`,`seventeen`,`eighteen`,`nineteen`,`twenty`

List of operations is 
`plus`, `minus`,`times`,`dividedby`

Words are case insensitive, example input `FIVE times TwEnTy`

Alternatively typing "quit" exits the program.



## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Requirements

Requires  `gcc`, `make`, `CMake`.

## Usage


# Create build directory and move into it
mkdir build && cd build

# Compile the program
make

# Run the compiled binary
./main


## Maintainers

Juho Kangas

