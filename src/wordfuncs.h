#pragma once
#include <string>
#include <algorithm>

std::string lowercaser(std::string input);
int toNumber(std::string word);
int toOperator(std::string word);