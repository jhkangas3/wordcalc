#include <iostream>
#include "wordfuncs.h"
#include <string>
#include <vector>
#include <sstream>



int main(){
    int num1; int num2;
    int caseOp;
    float x;
    std::string input;
    std::vector<std::string> wordArray;
    std::string val;
    

    while(true){
        std::cout<<"Input your calculation: \n";
        std::getline(std::cin, input);
        // String gets converted to string stream to be operated on
        std::stringstream streamData(input);
        // String gets chopped to individual words separated by spaces, and added to a vector
        while (std::getline(streamData, val, ' ')) {
            wordArray.push_back(val);
        }

        
        // First word checked with function toNumber
        num1 = toNumber(wordArray[0]);
        // -2 is returned when user types "quit"
        if(num1 == -2){
            std::cout<<"program closing \n";
            return 0;
        } std::cout<<"program closing \n";
        // If not enough words separated by spaces were supplied
        if (wordArray.size() < 3){
            std::cout<<"not enough input arguments \n";
            continue;
        }
        num2 = toNumber(wordArray[2]);
        // Input converted to int to select specified operation in switch case
        caseOp = toOperator(wordArray[1]);
        // One or more of the words didn't match the specified words
        if(num1 == -1 || num2 == -1 || caseOp == -1){
            std::cout<<"one of the inputs was invalid \n";
            wordArray.clear();
            continue;
        }
        // Case selection based on user input
        switch(caseOp){
            case 1 :// Additition
                std::cout<<num1<<"+"<<num2<<"="<<num1+num2<<std::endl;
                break;
            case 2: //substraction
                std::cout<<num1<<"-"<<num2<<"="<<num1-num2<<std::endl;
                break;
            case 3: //multiplying
                std::cout<<num1<<"*"<<num2<<"*"<<num1*num2<<std::endl;
                break;
            case 4: //division
                x = (float)num1 / (float)num2;
                std::cout<<num1<<"/"<<num2<<"="<<x<<std::endl;
                break;
            default:
                std::cout<<"we shouldn't get here \n";





        }


        

        break;
    }
    


    return 0;
}