#include "wordfuncs.h"

// This function converts words to lower case and removes spaces
std::string lowercaser(std::string input){

    std::transform(input.begin(), input.end(), input.begin(), ::tolower);
    std::remove(input.begin(), input.end(), ' ');
    return input;
}

// This function converts input word into integer, 
// or returns -1 if word was not one of the specified numbers
int toNumber(std::string word){
    word = lowercaser(word);

    if(word == "zero"){
        return 0;
    }else if(word == "one"){
        return 1;
    }else if(word == "two"){
        return 2;
    }else if(word == "three"){
        return 3;
    }else if(word == "four"){
        return 4;
    }else if(word == "five"){
        return 5;
    }else if(word == "six"){
        return 6;
    }else if(word == "seven"){
        return 7;
    }else if(word == "eight"){
        return 8;
    }else if(word == "nine"){
        return 9;
    }else if(word == "ten"){
        return 10;
    }else if(word == "eleven"){
        return 11;
    }else if(word == "twelve"){
        return 12;
    }else if(word == "thirteen"){
        return 13;
    }else if(word == "fourteen"){
        return 14;
    }else if(word == "fifteen"){
        return 15;
    }else if(word == "sixteen"){
        return 16;
    }else if(word == "seventeen"){
        return 17;
    }else if(word == "eighteen"){
        return 18;
    }else if(word == "nineteen"){
        return 19;
    }else if(word == "twenty"){
        return 20;
    }else if(word == "quit"){
        return -2;
    }

    return -1;

}

// This function converts operator word to integer for switch case in main
int toOperator(std::string word){

    word = lowercaser(word);
    if(word == "plus"){
        return 1;
    }else if(word == "minus"){
        return 2;
    }else if(word == "times"){
        return 3;
    }else if(word == "dividedby"){
        return 4;
    }
    

    return -1;
}